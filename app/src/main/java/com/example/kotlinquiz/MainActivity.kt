package com.example.kotlinquiz

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    var score: Int = 0
    var questionId = 0
    var questions = listOf(
        "What is a way to restrict direct access to some of an object’s fields. \n\n A) Private \n\n B) Encapsulation \n\n C) ViewModel",
        "What should be used to define a function in Kotlin? \n\n A) method \n\n B) fun \n\n C) function",
        "What represents, by default, the items in a row or column when using GridLayout? \n\n A) span \n\n B) list \n\n C) grid",
        "What should be used for long-running tasks to keep the UI running smoothly. \n\n A) threads \n\n B) linear layout \n\n C) coroutines")

    var correctAnswers = listOf(2, 2, 1, 3)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        answer_A.setOnClickListener { checkAnswer(1) }
        answer_B.setOnClickListener { checkAnswer(2) }
        answer_C.setOnClickListener { checkAnswer(3) }

        score_text.text = score.toString()
        question_text.text = questions[questionId]
    }

    private fun showNextQuestion() {
        questionId += 1
        if (questionId < questions.size) {
            question_text.text = questions[questionId]
        } else {
            question_text.text = "Game Over! Your score:"
        }
    }

    private fun checkAnswer(answer: Int) {
        if (questionId < questions.size) {
            if (answer == correctAnswers[questionId]) {
                score++
                Toast.makeText(applicationContext, "YES!", Toast.LENGTH_SHORT).show()
            } else {
                score--
                Toast.makeText(applicationContext, "NO!", Toast.LENGTH_SHORT).show()
            }
            score_text.text = score.toString()
            showNextQuestion()
        }
        else {
            // Game over
        }
    }




}